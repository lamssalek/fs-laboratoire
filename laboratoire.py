#Les opérations sur le laboratoire
import json

class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass
class AbsentException(LaboException):
    pass
class PresentException(LaboException):
    pass

def Laboratoire():
    return {}

def sauvegardejson(labo):
    lab = open("/home/majid/workspace/laboratoire/labo.json", "w")
    json.dump(labo,lab)

def chargerjson():
    with open('/home/majid/workspace/laboratoire/labo.json') as json_data:
        labo = json.load(json_data)
        print(labo)
    
def enregistrer_arrivee(labo, nom, bureau):
    if nom in labo:
        raise PresentException("Le nom est déjà enregistrer dans ce bureau")
    labo[nom]=bureau

def enregistrer_depart(labo, nom):
    if nom not in labo:
        raise AbsentException("Ce nom n'a pas été enregistrer dans le labo")
    labo.pop(nom, "Bureau Libéré")

def modifier_bureau(labo, nom, bureau):
    if nom not in labo:
        raise AbsentException("Ce nom n'a pas été enregistrer dans le labo")
    for key, value in labo.items():
        if key == nom:
            labo[nom] = bureau

def modifier_nom(labo, nom, newnom):
    if nom not in labo:
        raise AbsentException("Ce nom n'a pas été enregistrer dans le labo")
    for key in labo.items():
        if key == nom:
            labo[nom] = labo[newnom]

def verifier_membre(labo, nom):
    return nom in labo

def statut_bureau(labo, bureau):
    nom = [k  for (k, val) in labo.items() if val == bureau]
    return nom